deqp_mock = executable('deqp-mock',
                       'deqp-mock.cc',
                       dependencies : [fs_dep],
                       install: false)

tests = [
  'pass',
  'fail',
  'crash',
  'missingtest',
]

wrap = find_program('deqp_runner-test-wrapper.sh')

foreach t : tests
  if (t == 'pass')
    expected_exit_code = '0'
  else
    expected_exit_code = '1'
  endif

  test('deqp-' + t,
    wrap,
    args: [
      # Args to the wrapper
      t,
      deqp_runner.full_path(),
      join_paths(meson.current_source_dir(), 'deqp-' + t + '-expected.txt'),
      expected_exit_code,

      # Args to deqp_runner
      '--deqp',
      deqp_mock.full_path(),
      '--caselist',
      join_paths(meson.current_source_dir(), 'deqp-caselist.txt'),
    ],
  )
endforeach

test('deqp-xfail',
  wrap,
  args: [
    'failcrash',
    deqp_runner.full_path(),
    join_paths(meson.current_source_dir(), 'deqp-xfail-expected.txt'),
    '0',

    '--deqp',
    deqp_mock.full_path(),
    '--caselist',
    join_paths(meson.current_source_dir(), 'deqp-caselist.txt'),
    '--xfail-list',
    join_paths(meson.current_source_dir(), 'deqp-xfail-list.txt'),
  ],
)

test('deqp-flake-fail',
  wrap,
  args: [
    'flake',
    deqp_runner.full_path(),
    join_paths(meson.current_source_dir(), 'deqp-flake-expected.txt'),
    '1',

    '--deqp',
    deqp_mock.full_path(),
    '--caselist',
    join_paths(meson.current_source_dir(), 'deqp-caselist.txt'),
  ],
)

test('deqp-flake-pass',
  wrap,
  args: [
    'flake',
    deqp_runner.full_path(),
    join_paths(meson.current_source_dir(), 'deqp-flake-expected.txt'),
    '0',

    '--deqp',
    deqp_mock.full_path(),
    '--caselist',
    join_paths(meson.current_source_dir(), 'deqp-caselist.txt'),
    '--allow-flakes=true',
  ],
)

test('deqp-skip',
  wrap,
  args: [
    'pass',
    deqp_runner.full_path(),
    join_paths(meson.current_source_dir(), 'deqp-skip-expected.txt'),
    '0',

    '--deqp',
    deqp_mock.full_path(),
    '--caselist',
    join_paths(meson.current_source_dir(), 'deqp-caselist.txt'),
    '--exclude-list',
    join_paths(meson.current_source_dir(), 'deqp-skip-list.txt'),
  ],
)

test('deqp-big-caselist',
  wrap,
  args: [
    'pass',
    deqp_runner.full_path(),
    join_paths(meson.current_source_dir(), 'deqp-big-expected.txt'),
    '0',

    '--deqp',
    deqp_mock.full_path(),
    '--caselist',
    join_paths(meson.current_source_dir(), 'deqp-big-caselist.txt'),
  ],
)
